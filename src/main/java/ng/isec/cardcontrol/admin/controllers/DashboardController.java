package ng.isec.cardcontrol.admin.controllers;

/**
 * Created by isec on 04/06/2018.
 */
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {

    @GetMapping(value ="/app/dashboard")
    public String showDashboardForm()
    {
        return "dashboard";
    }
}
