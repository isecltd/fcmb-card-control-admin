package ng.isec.cardcontrol.admin.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by isec on 27/06/2018.
 */
@Controller
public class TransactionController {

    @GetMapping(value="/app/trxns")
    public String showTrxnForm()
    {
        return "cardSetting";
    }
}
