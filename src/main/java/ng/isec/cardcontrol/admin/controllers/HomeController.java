package ng.isec.cardcontrol.admin.controllers;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.*;

//import org.springframework.security.authentication.AnonymousAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.User;

/**
 * Created by isec on 30/05/2018.
 */
@Controller
public class HomeController {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(HomeController.class);
    private final List<String> isoCountriesList = Arrays.asList( Locale.getISOCountries());
    private final List<String> countryNames = this.getCountries();

    @GetMapping(path = "/report")
    public String show(Model model, HttpServletRequest httpServletRequest)
    {
        return "report";
    }

@GetMapping(path = {"/","/signIn"})
    public String home(Model model, HttpServletRequest httpServletRequest)
    {
//        CardSettingsDTO cardSettingsDTO = new CardSettingsDTO();
//        cardSettingsDTO.setIsoCountriesList(isoCountriesList);
//        cardSettingsDTO.setChannelList(Arrays.asList(Channel.values()));
//        cardSettingsDTO.setCountryNames(countryNames);
//
//
//        model.addAttribute("newCard",cardSettingsDTO);

//        return "cardSetting";
//        if(getAdminUser() != null)
//        {
//            return "redirect:/app/dashboard";
//        }
        return "dashboard";
    }

//   private AdminUser getAdminUser()
//   {
//       Authentication auth = SecurityContextHolder.getContext()
//               .getAuthentication();
//       AdminUser adminUser = null;
//
//       String domain = null;
//       if (auth != null && !auth.getClass().equals(AnonymousAuthenticationToken.class)) {
//           User user = (User) auth.getPrincipal();
//           adminUser = new AdminUser();
//           logger.info("Logged In User. "+user.getUsername());
//           adminUser.setUsername(user.getUsername());
//
//       }
//       return adminUser;
//   }
    private List<String> getCountries()
    {
        List<String> countriesList = new ArrayList<>();

        isoCountriesList.stream().forEach((code)->
        {
            countriesList.add( new Locale("",code).getDisplayCountry());
        });
        Collections.sort(countriesList);
        return countriesList;
    }
}
