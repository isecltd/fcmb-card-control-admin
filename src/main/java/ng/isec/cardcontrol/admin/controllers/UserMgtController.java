package ng.isec.cardcontrol.admin.controllers;

import ng.isec.go.admin.domain.AdminUser;
import ng.isec.go.admin.domain.UserRoles;
import ng.isec.go.admin.service.AdminUserRepository;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by isec on 30/05/2018.
 */
@Controller
public class UserMgtController {

    @Autowired
    private AdminUserRepository adminUserRepository;

    @ModelAttribute(name="rolesList")
    public List<UserRoles> userRoles()
    {
        return Arrays.asList(UserRoles.values());
    }

    @GetMapping(value="/app/createNewUser")
   public String showNewUserForm(HttpServletRequest httpServletRequest, Model model)
   {
       AdminUser adminUser = adminUserRepository.findOne(7l);
       model.addAttribute("adminUser", adminUser);
       return "newAdminUser";
   }

   @PostMapping(value="/app/createNewUser")
   public String processNewUser(BindingResult bindingResult, @ModelAttribute(name="newAdminUser") AdminUser adminUser, HttpServletRequest httpServletRequest)
   {
       if(bindingResult.hasErrors())
       {
           return "createUser";
       }
       return "createUser";
   }


    @RequestMapping(method = RequestMethod.GET, value = "/app/editUser")
    public String editUser(HttpServletRequest httpServletRequest, Model model)
    {

        return "editUser";
    }

    @RequestMapping(method = RequestMethod.POST,value = "/app/editUser")
    public String processEditUser(BindingResult bindingResult, @ModelAttribute(name = "editUser") AdminUser adminUser, HttpServletRequest httpServletRequest)
    {
        return "";
    }
}
