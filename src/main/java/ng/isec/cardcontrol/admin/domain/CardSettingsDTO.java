package ng.isec.cardcontrol.admin.domain;

import java.util.List;
import java.util.Map;

/**
 * Created by isec on 09/01/2019.
 */
public class CardSettingsDTO extends CardControlSettings{

    private List<Channel> channelList;

    private List<String> isoCountriesList;

    private List<String> countryNames;

    private Map<String, List<Channel>> allowedCountriesChannels;

    public List<Channel> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<Channel> channelList) {
        this.channelList = channelList;
    }

    public List<String> getIsoCountriesList() {
        return isoCountriesList;
    }

    public void setIsoCountriesList(List<String> isoCountriesList) {
        this.isoCountriesList = isoCountriesList;
    }

    public List<String> getCountryNames() {
        return countryNames;
    }

    public void setCountryNames(List<String> countryNames) {
        this.countryNames = countryNames;
    }

    public Map<String, List<Channel>> getAllowedCountriesChannels() {
        return allowedCountriesChannels;
    }

    public void setAllowedCountriesChannels(Map<String, List<Channel>> allowedCountriesChannels) {
        this.allowedCountriesChannels = allowedCountriesChannels;
    }
}
