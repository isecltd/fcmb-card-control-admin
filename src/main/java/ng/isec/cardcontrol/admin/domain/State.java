package ng.isec.cardcontrol.admin.domain;

/**
 * Created by isec on 03/04/2018.
 */
public enum State {
    AB("ABIA"), AD("ADAMAWA"),AK("AKWA IBOM"),AN("ANAMBRA"), BA("BAUCHI"),BY("BAYELSA"), BE("BENUE"), BO("BORNO"), CR("CROSS_RIVER"), DE("DELTA"),
    EB("EBONYI"), ED("EDO"),EK("EKITI"),EN("ENUGU"), GO("GOMBE"), IM("IMO"),JG("JIGAWA"), KD("KADUNA"), KN("KANO"), KT("KATSINA"),KB("KEBBI"),KG("KOGI"),
    KW("KWARA"), LG("LAGOS"),NS("NASARRAWA"), NG("NIGER"), OG("OGUN"), OY("OYO"), OS("OSUN"), ON("ONDO"), PL("PLATEAU"), RV("RIVERS"), SK("SOKOTO"),
    TR("TARABA"), YB("YOBE"),ZM("ZAMFARA") , UNKNOWN("UNKNOWN") ;

    private String alias;

    State( String alias)
    {
        this.alias = alias;
    }

    @Override
    public String toString()
    {
        return alias;
    }

    public static State value(String state)
    {
        try
        {
            return valueOf(state);

        }
        catch(IllegalArgumentException exception)
        {
            return State.UNKNOWN;
        }
    }

}
