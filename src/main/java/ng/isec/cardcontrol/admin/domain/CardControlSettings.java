package ng.isec.cardcontrol.admin.domain;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name="CARDS")
public class CardControlSettings extends RootEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="MASKEDPAN")
	private String pan;

	@Column(name = "ENCRYPTEDFULLPAN")
	private String encryptedFullPan;

//	@Column(name="ACCTNUMBER")
	@Transient
	private String acctNumber;
	
	@Column(name="BLOCKED")
	private boolean blocked;
	
//	@Column(name="ACTIVATED")
	@Transient
	private boolean activated;
	
	@Column(name="ALLOWEDCOUNTRIES")
	private String allowedCountries;
	
	@Column(name="DAILYLIMIT")
	private BigDecimal dailyLimit;

	@Column(name="MONTHLYLIMIT")
	private BigDecimal monthlyLimit;

	@Column(name="USEDDAILYLIMIT")
	private BigDecimal usedDailyLimit;

	@Column(name="USEDMONTHLYLIMIT")
	private BigDecimal usedMonthlyLimit;

	@Transient

	private BigDecimal usedLifeTimeLimit;

	@Transient

	private BigDecimal lifeTimeLimit;

	@Column(name="CUSTID")
	private String custID;

	
	@Transient
	private List<Country> countriesList;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getAcctNumber() {
		return acctNumber;
	}
	public void setAcctNumber(String acctNumber) {
		this.acctNumber = acctNumber;
	}
	public boolean isBlocked() {
		return blocked;
	}
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
	public boolean isActivated() {
		return activated;
	}
	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	public String getAllowedCountries() {
		return allowedCountries;
	}
	public void setAllowedCountries(String allowedCountries) {
		this.allowedCountries = allowedCountries;
	}
	public BigDecimal getDailyLimit() {
		return dailyLimit;
	}
	public void setDailyLimit(BigDecimal dailyLimit) {
		this.dailyLimit = dailyLimit;
	}
	public BigDecimal getMonthlyLimit() {
		return monthlyLimit;
	}
	public void setMonthlyLimit(BigDecimal monthlyLimit) {
		this.monthlyLimit = monthlyLimit;
	}
	public BigDecimal getUsedDailyLimit() {
		return usedDailyLimit;
	}
	public void setUsedDailyLimit(BigDecimal usedDailyLimit) {
		this.usedDailyLimit = usedDailyLimit;
	}
	public BigDecimal getUsedMonthlyLimit() {
		return usedMonthlyLimit;
	}
	public void setUsedMonthlyLimit(BigDecimal usedMonthlyLimit) {
		this.usedMonthlyLimit = usedMonthlyLimit;
	}
	public BigDecimal getUsedLifeTimeLimit() {
		return usedLifeTimeLimit;
	}
	public void setUsedLifeTimeLimit(BigDecimal usedLifeTimeLimit) {
		this.usedLifeTimeLimit = usedLifeTimeLimit;
	}
	public BigDecimal getLifeTimeLimit() {
		return lifeTimeLimit;
	}
	public void setLifeTimeLimit(BigDecimal lifeTimeLimit) {
		this.lifeTimeLimit = lifeTimeLimit;
	}
	public List<Country> getCountriesList() {
		return countriesList;
	}
	public void setCountriesList(List<Country> countriesList) {
		this.countriesList = countriesList;
	}

	public String getEncryptedFullPan() {
		return encryptedFullPan;
	}

	public void setEncryptedFullPan(String encryptedFullPan) {
		this.encryptedFullPan = encryptedFullPan;
	}

	public String getCustID() {
		return custID;
	}

	public void setCustID(String custID) {
		this.custID = custID;
	}

	public void makeCountryList()
	{
		
		ObjectMapper objectMapper= new ObjectMapper();
		try {
			if(this.getAllowedCountries() != null )
			{
				List<Country>  countries = objectMapper.readValue(this.getAllowedCountries(), new TypeReference<List<Country>>(){});
				countries.stream().forEach((country) ->
				{
					System.out.printf(" %s \n", country.getTwoDigitCode());
					country.getAllowedTransactionChannels().stream().forEach((channel -> {System.out.printf("%s",channel.name());}));
				} );
				this.setCountriesList(countries);
			}

			
		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	

	
}
