package ng.isec.cardcontrol.admin.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by isec on 13/06/2018.
 */
@Entity
@Table(name="AUDITRAIL")
public class CardControlAudit implements Serializable{

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "EVENT")
    private String event;

    @Column(name = "DESCRIPTION")
    private String description;


    @Column(name = "custID")
    private String custID;

    @Column(name = "PAN")
    private String pan;

    @Column(name="updateddate")
    @Temporal(value = TemporalType.DATE)
    private Date lastUpdateDate;

    @Column(name="IP_ADDRESS")
    private String ipAddress;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "FUNCTION_NAME")
    private String function;

    @Column(name = "HOSTNAME")
    private String hostname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getCustID() {
        return custID;
    }

    public void setCustID(String custID) {
        this.custID = custID;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
}
