package ng.isec.cardcontrol.admin.domain;

/**
 * Created by isec on 12/12/2018.
 */
public enum UserRoles {

    ROLE_CONTROL_MAKER("Control Maker"),
    ROLE_CONTROL_CHECKER("Control Checker"),
    ROLE_CONTACT_CENTER_MAKER("Contact Center Maker"),
    ROLE_CONTACT_CENTER_CHECKER("Contact Center Checker"),
    ROLE_SUPPORT("Support");

    private UserRoles(String roleName)
    {
        this.roleName = roleName;
    }

    private final String roleName;

    public String getRoleName()
    {
        return this.roleName;
    }
}
