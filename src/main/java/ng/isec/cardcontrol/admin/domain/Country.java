package ng.isec.cardcontrol.admin.domain;

import java.util.List;

public class Country extends RootEntity {
	
	private String twoDigitCode;
	private List<Channel> allowedTransactionChannels;
	private List<State> allowedStates;
	private String countryName;

	public String getTwoDigitCode() {
		return twoDigitCode;
	}

	public void setTwoDigitCode(String twoDigitCode) {
		this.twoDigitCode = twoDigitCode;
	}

	public List<Channel> getAllowedTransactionChannels() {
		return allowedTransactionChannels;
	}

	public void setAllowedTransactionChannels(List<Channel> allowedTransactionChannels) {
		this.allowedTransactionChannels = allowedTransactionChannels;
	}

	public List<State> getAllowedStates() {
		return allowedStates;
	}

	public void setAllowedStates(List<State> allowedStates) {
		this.allowedStates = allowedStates;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
}
