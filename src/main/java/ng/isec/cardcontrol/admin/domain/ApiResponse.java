package ng.isec.cardcontrol.admin.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by isec on 10/01/2019.
 */
@JsonDeserialize
public class ApiResponse {
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
