package ng.isec.cardcontrol.admin.repo;

import ng.isec.cardcontrol.admin.domain.CardTransactionResponse;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by isec on 16/02/2018.
 */
public interface CardTransactionResponseRepository extends
        JpaRepository<CardTransactionResponse, Integer> {

    public CardTransactionResponse findByAcctNumberAndRrnNumberAndPanEndsWith(String acctNumber, String rrnNumber, String pan);
}
