package ng.isec.cardcontrol.admin.repo;

import ng.isec.cardcontrol.admin.domain.CardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CardTransactionRepository extends
		JpaRepository<CardTransaction, Integer> {

	public List<CardTransaction> findByAcctNumberAndPanEndsWithOrderByUpdatedDateDesc(String acctNumber, String pan);

	@Query(value = "select top 1000 * from cardtranx",nativeQuery = true)
	public List<CardTransaction> findTopAll();


}
