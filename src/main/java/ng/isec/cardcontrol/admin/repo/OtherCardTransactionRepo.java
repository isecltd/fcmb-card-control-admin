package ng.isec.cardcontrol.admin.repo;

import ng.isec.cardcontrol.admin.domain.OtherCardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by isec on 12/12/2018.
 */
@Repository
public interface OtherCardTransactionRepo extends JpaRepository< OtherCardTransaction, Integer> {
}
