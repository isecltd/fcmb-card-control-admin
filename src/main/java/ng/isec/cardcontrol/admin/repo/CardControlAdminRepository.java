package ng.isec.cardcontrol.admin.repo;

import ng.isec.cardcontrol.admin.domain.CardControlAdminUser;
import ng.isec.cardcontrol.admin.domain.CardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CardControlAdminRepository extends JpaRepository<CardControlAdminUser, Integer>{

	default List<CardTransaction> getAllTransactions()
	{
		return null;
	}

	@Query("From CardControlAdminUser admUser where admUser.username=? AND admUser.enabled=1")
   public Optional<CardControlAdminUser> findAdminUser(String username);

	public Optional<CardControlAdminUser> findByUsername(String username);

}
