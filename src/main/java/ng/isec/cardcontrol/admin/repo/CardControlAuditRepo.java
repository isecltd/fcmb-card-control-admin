package ng.isec.cardcontrol.admin.repo;

import ng.isec.cardcontrol.admin.domain.CardControlAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by isec on 13/06/2018.
 */

public interface CardControlAuditRepo extends JpaRepository<CardControlAudit, Long> {
    @Query(value = "select top 100 * from AUDITRAIL order by updateddate desc",nativeQuery = true)
    public List<CardControlAudit> findTopAll();
}
