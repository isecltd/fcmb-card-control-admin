package ng.isec.cardcontrol.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackages = {"hello","configuration","controllers"})
public class AdminApplication {

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(AdminApplication.class, args);
    }

}
