package ng.isec.cardcontrol.admin.service;

import ng.isec.go.admin.domain.AdminUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by isec on 17/01/2019.
 */
@Repository
public interface AdminUserRepository extends JpaRepository<AdminUser, Long> {
    public AdminUser findByUsername(String username);
}
