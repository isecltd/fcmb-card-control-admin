package ng.isec.cardcontrol.admin.service;//package ng.isec.go.admin.service;
//
//import ng.isec.go.admin.domain.AdminUser;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collection;
//
///**
// * Created by isec on 17/01/2019.
// */
//@Service
//public class AdminUserDetailsService implements  UserDetailsService{
//
//    private final Logger logger = LoggerFactory.getLogger(AdminUserDetailsService.class);
//
//    private AdminUserRepository adminUserRepository;
//
//    @Autowired
//    public AdminUserDetailsService(AdminUserRepository adminUserRepository) {
//        this.adminUserRepository = adminUserRepository;
//        logger.info("Invoking AdminUserDetailsService Constructor");
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        logger.info("Invoking loadUserByUsername"+" Username"+username);
//
//        AdminUser user = this.adminUserRepository.findByUsername(username);
//
//        if(user == null)
//        {
//            throw new UsernameNotFoundException("Invalid Username/Password.");
//        }
//        logger.info("Retrieved User: "+user.getUsername()+" Retrieved Password"+user.getPassword());
//
//        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(user.getRoleName());
//       User user1 = new User(user.getUsername(),user.getPassword(), Arrays.asList(grantedAuthority));
//       user1.getAuthorities().stream().forEach((authority)->
//       {
//           logger.info("Authorities: "+authority);
//       });
//        return user1;
//    }
//}
