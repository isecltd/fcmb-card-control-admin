 $(function () {
   function initEventBus() {
     var eb = new EventBus("http://connect.isec.ng/connect");
     eb.onopen = function () {

       console.log("Eventbbus connected");
       var transactionHolder = $('#transactionList');

       eb.registerHandler("out.log", null, function (err, msg) {
         console.log(msg);
       });

       var getTag = function (key, location) {
         return 'data-tag="' + key + '::' + location + '"';
       };



       var getProgressByStatus = function (status) {
         var status;
         switch (status) {
           case 'COMPLETED':
           case 'SUCCESSFUL':
             status = 'success';
             break;
           case 'FAILED':
             status = 'danger';
             break;
           case 'PENDING':
           default:
             status = 'warning';
             break;
         }
         return '<div  class="progress-bar progress-bar-'+status+'" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>';
       }


       var getIconByStatus = function (status) {
         var status;
         switch (status) {
           case 'COMPLETED':

           case 'SUCCESSFUL':

             status = 'success';

             break;

           case 'FAILED':

             status = 'fail';

             return '<i class="icon ion-close-circled ionic-' + status + '"></i>';

             case 'INITIAL':
             status = 'default';

             return '<i class="icon ion-minus-circled ionic-' + status + '"></i>';
            
           case 'PENDING':
           default:
             status = 'pending';
             break;
         }
         return '<i class="icon ion-checkmark-circled ionic-' + status + '"></i>';
       }

       eb.registerHandler("monitoring.localbank.log", null, function (err, msg) {
         var id = msg.body.transactionID;
         var request = msg.body.request;
         console.log(request.status)
         console.log(request);
         var html = `<tr ` + getTag(id, 'id') + `>
         <td><a>` + id + `</a></td>
         <td>` + request.account_number + `</td>
         <td>` + request.transaction_amount + `</td>
          <td>` + request.time + `</td>
          <td ` + getTag(id, 'local') + `>` + getIconByStatus(msg.body.status) + `</td>
          <td ` + getTag(id, 'cloud') + `>` + getIconByStatus('PENDING') + `</td>
          <td ` + getTag(id, 'push') + `>` + getIconByStatus('PENDING') + `</td>
          <td ` + getTag(id, 'ussd') + `>` + getIconByStatus('INITIAL') + `</td>
          <td>
          <div class="progress progress-pad" `+getTag(id, 'status')+`>
            <div  class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
          </div>
          </td>
        </tr>`;
         if ($('[' + getTag(id, 'id') + ']').length) {
           $('[' + getTag(msg.body.transactionID, 'local') + ']').html(getIconByStatus(msg.body.status));
           $('[' + getTag(msg.body.transactionID, 'status') + ']').html(getProgressByStatus(msg.body.request.status));
         } else {
           transactionHolder.prepend($(html));
         }
       });

       eb.registerHandler("clients.log", null, function (err, msg) {
         console.log("clients.log : " + JSON.stringify(msg));
       });

       eb.registerHandler("monitoring.cloud.log", null, function (err, msg) {
         $('[' + getTag(msg.body.transactionID, 'cloud') + ']').html(getIconByStatus(msg.body.status));
       });

       eb.registerHandler("monitoring.ussd.log", null, function (err, msg) {
         $('[' + getTag(msg.body.transactionID, 'ussd') + ']').html(getIconByStatus(msg.body.status));
       });

       eb.registerHandler("monitoring.push.log", null, function (err, msg) {
         $('[' + getTag(msg.body.transactionID, 'push') + ']').html(getIconByStatus(msg.body.status));
       });
     };
   }

   initEventBus();
 });